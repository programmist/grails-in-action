package qotd

class QuoteController {

    def index() {
        redirect action: "home"
    }

    def home() {
        render "<h1>Home</h1>"
    }

    def random() {
        def staticAuthor = "Anonymous"
        def staticContent = "Test content"
        [ author: staticAuthor, content: staticContent]
    }
}
